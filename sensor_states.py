import time
import numpy as np
import pandas as pd
import json
from pandas.io.json import json_normalize
import os
import datetime as dt
from random import randint
##test comment

def mapData(df):
    df = df.rename(columns={'NOV.RigSense.General Time-Based.DEPTBITM': 'Bit Depth',
                            'NOV.RigSense.General Time-Based.BLKPOS': 'Block Position',
                            'NOV.RigSense.General Time-Based.MFIA': 'Flow In',
                            'NOV.RigSense.General Time-Based.HKLA': 'Hook Load',
                            'NOV.RigSense.General Time-Based.SPPA': 'Standpipe Pressure',
                            'NOV.RigSense.General Time-Based.TDTORQ': 'TD Torque',
                            'NOV.RigSense.General Time-Based.TDRPM': 'TD Speed',
                            'NOV.RigSense.General Time-Based.DEPTMEAS': 'Total Depth',
                            'NOV.RigSense.General Time-Based.WOBA': 'Wob'})
    return df




def isCloseToZero(value, threshold = 0.004):
    return (np.abs(value)<threshold)

def getConstantDirection(df,direction, cColumn, cThreshold):
    changePointList = []
    prevDirection = -1
    ##df[cColumn].to_csv(dir+'obs.csv')
    for i in range(0, len(df[cColumn])):
        """print cThreshold
        print i
        print df[cColumn][i]"""
        if(abs(df[cColumn][i]) > cThreshold):
            direction[cColumn][i] = 1
        else:
            direction[cColumn][i] = 0

    return changePointList

def identifyDirection(cList, direction, cColumn, minDip, cMinThreshold, maxConstLength, minLength, slope, zeroThreshold = 0.002):
    inPositiveZone = False
    inConstantZone = False
    constantLength = 0
    inPossiblePositiveZone = False
    startPositiveZone = -2
    lenDirection = len(cList)

    for i in range(1, lenDirection):
        difference = slope*(cList[i]-cList[i-1])

        if(isCloseToZero(difference,zeroThreshold)):
           inConstantZone = True
           constantLength = constantLength+1
        else:
            inConstantZone = False
            constantLength = 0
        if(difference >= cMinThreshold):
            inPositiveZone = True;
        if(difference > 0):
            if(inPossiblePositiveZone != True):
                inPossiblePositiveZone = True;
                startPositiveZone = i
        if((difference < 0 and isCloseToZero(difference, zeroThreshold)== False) or constantLength >= maxConstLength or i == lenDirection -1):
            if(inPossiblePositiveZone):
                inPossiblePositiveZone = False
            if(inPositiveZone):
                inPositiveZone = False;
                if(constantLength >0):
                    endPoint = i - constantLength
                else:
                    endPoint = i-1
                if(difference >=0 and i == lenDirection-1):
                    endPoint = i

                totalFall = slope*(cList[endPoint]-cList[startPositiveZone-1])
                totalDuration = endPoint - startPositiveZone +1
                if(totalDuration >= minLength) and (totalFall >= minDip):
                    for k in range(startPositiveZone, endPoint+1):
                        direction[cColumn][k] = slope

def createDirectionIndices(df, original, cColumn):
    print ('In funciton createDirectionIndices')
    data = []
    obj = {
        "start_index": -1,
        "start_time": -1,
        "end_index": -1,
        "direction": -1,
        "block_diff": -100
    }
    length = 1
    currentState = -1

    state = df[cColumn].values
    blockPosition = original['Block Position'].values
    flag = False

    for i in range(0, len(df)):
        if i == 0:
            obj["start_index"] = i
            obj["start_time"] = df.index[i]
            obj["direction"] = state[i]
            currentState = state[i]
            continue
        if i == len(df)-1:
            obj["end_index"] = i
            obj["block_diff"]= blockPosition[obj["end_index"]]-blockPosition[obj["start_index"]]
            data.append(obj)

            return data

        if state[i] != currentState:
            obj["end_index"] = i-1
            obj["block_diff"]= blockPosition[obj["end_index"]]-blockPosition[obj["start_index"]]
            #print obj['direction']
            #print obj['block_diff']
            data.append(obj)
            obj = {
                "start_index": -1,
                "end_index": -1,
                "start_time": -1,
                "direction": -1
            }
            length = length + 1
            obj["start_index"] = i
            obj["direction"] = state[i]
            obj["start_time"] = df.index[i]

            currentState = state[i]

    return data

def killZeros(direction, df, cColumn):
    #print cColumn

    data = createDirectionIndices(direction, df, cColumn)
    length = len(data)
    if (cColumn == 'Bit Depth') or (cColumn == 'Block Position') or (cColumn == 'Wob'):
        for i in range(0, length):
            if (data[i]['direction'] != 0):
                if (data[i]['direction'] * data[i]['block_diff'] > -0.002):
                    for j in range(data[i]['start_index'], data[i]['end_index'] + 1):
                        direction[cColumn][j] = 0
                        data[i]['direction'] = 0

    if (length > 2):
        for i in range(1, length - 1):

            if (data[i]['direction'] == 0):
             if (data[i - 1]['direction'] == data[i + 1]['direction']):
                    zeroLenState = data[i]['end_index'] - data[i]['start_index'] + 1
                    prevLenState = data[i - 1]['end_index'] - data[i - 1]['start_index'] + 1
                    futureLenState = data[i + 1]['end_index'] - data[i + 1]['start_index'] + 1
                    ratio = zeroLenState / float(prevLenState + futureLenState)
                    if (ratio < 0.6 and zeroLenState < 15):
                        for j in range(data[i]['start_index'], data[i]['end_index'] + 1):
                            direction[cColumn][j] = data[i - 1]['direction']



def identifyDirectionOfChannels(df, direction,df_config):
    #maxConstLength = 4
    #minLength = 3
    #cMinThreshold = 0.0005
    #minDip = 0.005
    #cColumn = 'Total Depth'
    cColumn = df_config['column'].values
    minDip = df_config['minDip'].values
    cMinThreshold = df_config['minThreshold'].values
    maxConstLength = df_config['maxConstLength'].values
    minLength = df_config['minLength'].values
    cPointsTotal = []
    for i in range(0,len(df_config)):
        if cColumn[i] == 'Total Depth' or cColumn[i] == 'Bit Depth' or cColumn[i] == 'Block Position' or cColumn[i] == 'Wob':
            column = df[cColumn[i]]
            identifyDirection(column, direction, cColumn[i], minDip[i], cMinThreshold[i], maxConstLength[i], minLength[i], -1,
                              cMinThreshold[i])
            identifyDirection(column, direction, cColumn[i], minDip[i], cMinThreshold[i], maxConstLength[i], minLength[i], 1,
                              cMinThreshold[i])
            killZeros(direction, df, cColumn[i])

        if cColumn[i] == 'Flow In' or cColumn[i] == 'Standpipe Pressure' or cColumn[i] == 'TD Torque'\
            or cColumn[i] == 'TD Speed':
            cPointsTotal = cPointsTotal + getConstantDirection(df, direction, cColumn[i], cMinThreshold[i])
    '''identifyDirection(df[cColumn], direction, cColumn, minDip, cMinThreshold, maxConstLength, minLength, -1,cMinThreshold)
    identifyDirection(df[cColumn], direction, cColumn, minDip, cMinThreshold, maxConstLength, minLength, 1,cMinThreshold)
    killZeros(direction, df, cColumn)'''


    #minDip = 0.1
    #cMinThreshold = 0.01
    #slope = -1
    #cColumn = 'Bit Depth'
    '''identifyDirection(df[cColumn], direction, cColumn, minDip, cMinThreshold, maxConstLength, minLength, -1)
    identifyDirection(df[cColumn], direction, cColumn, minDip, cMinThreshold, maxConstLength, minLength, 1)
    killZeros(direction, df, cColumn)'''

    #columns = ['Flow In', 'Standpipe Pressure', 'TD Torque', 'TD Speed']
    #thresholds = [0.001, 1000000, 20, 0.1]
    '''cPointsTotal = []
    for i in range(0,4):
            cPointsTotal = cPointsTotal + getConstantDirection(df, direction, columns[i], thresholds[i])'''
    #direction.to_csv("C:/Users/avisw/Desktop/Rowan/results/testResutsNew20.csv")




#directions of the Hook Load must be given. There will now be two threshholds, obtained
#from hookLoad threshhold. If it falls below the HL threshhold, it is in slips until
#it elevates hl_upper above the HL threshhold. If the dataset starts somewhere between the
#two threshholds, find the first location where it falls either below the HL or
#above the HL*hl_upper and mark all previous points relative to this point (1 or 0).



hl_upper = 1

def hook_load_directions(direction,df,hookLoad):
    hl = df['Hook Load'].tolist()
    hl_direction = direction['Hook Load'].tolist()
    time = 0
    if hl_upper==1:
        while time < len(hl):
            if (hl[time] < hookLoad):
                hl_direction[time] = 1
            else:
                hl_direction[time] = 0
            time += 1
        direction['Hook Load'] = hl_direction
        return direction
    elif hl_upper!=1:
        while time < len(hl):
            #if at time 0, you are in between the threshholds, find the first point
            #where it is above or below a threshhold.
            if time==0 and hl[time]<=hookLoad*hl_upper and hl[time]>=hookLoad:
                fp = next((j for j in range(time,len(hl)) if (hl[j]>=hookLoad*hl_upper or hl[j]<=hookLoad)),len(hl))
                #if the first point equals the length of the df, mark all as in slips and
                #end loop
                if fp==len(hl):
                    hl_direction = [1]*len(hl)
                    break
                #if the first point found is above the HL*hl_upper, mark all previous as
                #not in slips, set the time to fp, and continue the loop
                elif hl[fp]>= hookLoad*hl_upper:
                    hl_direction[0:fp] = [0]*fp
                    time = fp
                    continue
                #If the first point found is below the HL, mark all previous as
                #in slips, set the time to fp, and continue the loop
                elif hl[fp]<= hookLoad:
                    hl_direction[0:fp] = [1]*fp
                    time = fp
                    continue
            #if the HookLoad at the time is >= HL*hl_upper, find the point where it is less
            #than the HL
            elif hl[time]>=hookLoad*hl_upper:
                fp = next((j for j in range(time,len(hl)) if hl[j]<=hookLoad),len(hl))
                #mark all points within interval as out of slips and set time to fp
                hl_direction[time:fp] = [0]*(fp-time)
                time = fp
                continue
            #if the HookLoad at the time is <= HL, find the point where it is greater
            #than the HL*hl_upper
            elif hl[time]<=hookLoad:
                fp = next((j for j in range(time,len(hl)) if hl[j]>=hookLoad*hl_upper),len(hl))
                #mark all points within interval as in slips and set time to fp
                hl_direction[time:fp] = [1]*(fp-time)
                time = fp
                continue
        direction['Hook Load'] = hl_direction
        return direction



def getStateFinal(direction,df_config,df):
    totalLength = len(direction)
    stateArray = []
    i = 0
    df_speed = df['TD Speed'].tolist()
    cColumn = df_config['column'].tolist()
    cMinThreshold = df_config['minThreshold'].tolist()
    Speed_threshold = cMinThreshold[cColumn.index("TD Rotating")]
    col_td = direction['Total Depth'].tolist()
    col_bd = direction['Bit Depth'].tolist()
    col_spp = direction['Standpipe Pressure'].tolist()
    col_speed = direction['TD Speed'].tolist()
    col_flow = direction['Flow In'].tolist()
    col_hook = direction['Hook Load'].tolist()
    df_bitdepth = df['Bit Depth'].tolist()
    ##### flow and spp threshold addition  and no data availabe addition 3/14/2018
    flow_threshold=cMinThreshold[cColumn.index("Flow In")]
    spp_threshold=cMinThreshold[cColumn.index("Standpipe Pressure")]
    df_flow=df['Flow In'].tolist()
    df_spp=df['Standpipe Pressure'].tolist()
    df_torque=df['TD Torque'].tolist()
    df_blkpos = df['Block Position'].tolist()
    df_hkla=df['Hook Load'].tolist()
    df_total_depth=df['Total Depth'].tolist()
    df_wob=df['Wob'].tolist()
    while(i < totalLength):
        state = 11
        if(col_td[i] == 1):
            state = 1
            if df_speed[i] < Speed_threshold:
                state = 10
            if df_flow[i]<flow_threshold and df_spp[i]<spp_threshold:
                state=15   #############other
        else:
            if(col_bd[i] == 0):
                if(col_spp[i] == 1 or col_flow[i] == 1):
                    state = 8
            elif(col_bd[i] == 1):
                if(col_speed[i] == 1 and col_flow[i] == 1):
                    state = 3
                elif(col_speed[i] == 0 and col_flow[i] == 1):
                    state = 5
                else:
                     state = 6
            elif(col_bd[i] == -1):
                if (col_speed[i] == 1 and col_flow[i] == 1):
                    state = 2
                elif (col_speed[i] == 0 and col_flow[i] == 1):
                    state = 4
                else:
                    state = 7
        if (col_hook[i] == 1 and (state!=1 or state!=10)):
            state = 0


        if df_torque[i]==-9999 and df_blkpos[i]==-9999 and df_spp[i]==-9999 and df_bitdepth[i]==-9999 and df_flow[i]==-9999 and df_hkla[i]==-9999 and df_speed[i]==-9999 and df_total_depth[i]==-9999 and df_wob[i]==-9999:
            state=14 #######No data available
        if df_torque[i]==-9998 and df_blkpos[i]==-9998 and df_spp[i]==-9998 and df_bitdepth[i]==-9998 and df_flow[i]==-9998 and df_hkla[i]==-9998 and df_speed[i]==-9998 and df_total_depth[i]==-9998 and df_wob[i]==-9998:
            state=14 #######No data available

        stateArray.append(state)
        i += 1
    return stateArray

def run(databroker, inputtags, model_name, logger):
    try :

        print ("in Model run method")
        csv_dir = str(os.getcwd())+ '/user/'

        start_t1 = pd.to_datetime(open(csv_dir+'previous_process_date.txt','r').read())

        curr_time = dt.datetime.now()

        curr_time = curr_time.replace(microsecond=0)

        get_values = pd.Timedelta(pd.to_datetime(curr_time)-(start_t1)).seconds
        #get_values = 5
        #### relatime limit #### 900 seconds
        if get_values > 900:
            get_values = 900



        df = databroker.get_last_n_values(get_values)
        logger.info(str(len(df)))
        #df.to_csv('test.csv')
        if not df.empty:
            #print ("pt1",df.head())
            #print (df.columns)
            x = pd.DataFrame()
            for d in df['iotgateway1']:
                # x= x.append(json_normalize(d),sort=True)
                z = json.loads(d)
                x = x.append(pd.DataFrame(data=z))
            #print (df.head())
            p = pd.DataFrame(columns=['q', 't', 'v', 'id'])
            #p.to_csv('test1.csv')
            for d in x['values']:
                p = p.append(json_normalize(d))
            # print (x)
            #p.to_csv('test2.csv')
            # if you want in different structure use below
            y = p.pivot(index='t', columns='id', values='v')
            #y.to_csv('test2_5.csv')
            #y.columns = y.columns.droplevel(0)
            #y.to_csv('test30.csv',index=False)
            start_time = str(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(y.index[0] // 1000)))

            end_time = str(time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(y.index[-1] // 1000 )))

            print (start_time, end_time)

            t1 = pd.date_range(start_time, end_time, freq="1S")  ## just to testing df_global
            #y.to_csv('test40.csv')
            df1 = pd.DataFrame(data=t1, columns=['Timestamp'])

            # print (len(y['NOV.RigSense.General Time-Based.BLKPOS'].values))

            # print (len(df1))
            #df1.to_csv('before_pre_1_4.csv')
            df1 = df1.iloc[0:len(y)]
            #print ("p2")
            print (len(df1))
            print (len(y))
            #df1.to_csv('before_pre_4')
            for col in y.columns:
                df1[col] = y[col].values

            #df = dataframe_generator.df_generator(90)
            #print (df.head())
            #print ("p3")
            #df1.to_csv('before_4.csv')
            df = mapData(df1)
            #df.to_csv('test4.csv')
            #print("Before")

            df = df[['Timestamp','TD Speed', 'TD Torque', 'Standpipe Pressure', 'Block Position', 'Bit Depth', 'Total Depth', 'Hook Load','Wob', 'Flow In']]
            df.set_index('Timestamp', inplace=True)

            df = df.loc[pd.to_datetime(start_t1)+pd.Timedelta(seconds=1):]
            #df.to_csv('test11.csv')
            end_time = df.index[-1]

            file = open(csv_dir+'previous_process_date.txt', 'w')
            file.write(str(end_time))
            file.close()

            #print ("After")
            #df = dataframe_generator.df_generator(90)

            ### for 22 ###

            df_config = pd.read_csv(csv_dir+ '22_config.csv')
            #df_config.to_csv('test11.csv')
            col = df_config['column'].values
            cMinThreshold = df_config['minThreshold'].values
            for i in range(0, len(df_config)):
                if col[i] == 'Hook_Load':
                    hookLoad = cMinThreshold[i]

            direction = pd.DataFrame(index=df.index, columns=df.columns)
            direction = direction.fillna(0)

            identifyDirectionOfChannels(df, direction, df_config)

            direction = hook_load_directions(direction, df, hookLoad)

            df['states'] = getStateFinal(direction,df_config,df)


            df['model_name'] = model_name
            ####added states ###3
            #df.to_csv('C:/Users/kriamm/Desktop/json_parse/test.csv')

            df.reset_index(level=0, inplace=True)
            # df['Timestamp'] = str(df['Timestamp'])


            #x = df.to_json(orient='records')[1:-1].replace('},{', '} {')
            #result = json.dumps(df.to_dict())

            return df
        else:
            print ("Empty dataframe")
    except Exception as e :

        logger.info ("In except")
        logger.info( str(e))
        x = { "Timestamp":str('2018-09-01'),"Exception":str(e)}
        #df1 = pd.DataFrame(x.items(), columns=['Timestamp', 'Exception'])
        return pd.DataFrame()
        #result = json.dumps(x)

        #return df1
